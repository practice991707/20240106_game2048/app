# Stage 1: Build the application
FROM node:22 as builder
WORKDIR /app
COPY package*.json .
RUN npm install
COPY . .
RUN npm run build-prod

# Stage 2: Execute with nginx
FROM nginx:1.25.4-alpine-slim
COPY --from=builder /app/dist /usr/share/nginx/html
HEALTHCHECK --interval=2s --timeout=1s --retries=1 --start-period=10s \
    CMD curl -sf "http://localhost" || exit 1
EXPOSE 80
CMD ["nginx","-g","daemon off;"]
